#!/bin/bash

extract_section() {
    local sct_start="$1"
    local sct_end="$2"
    local sub_sct

    sub_sct=$(sed --quiet "/$sct_start/,/$sct_end/p" MAINTAINERS)
    echo "$sub_sct" | grep -v "\($sct_start\|$sct_end\|^\$\)"
}

count_items_maintainers() {
    local section="$1"

    echo "$section" | cut -f 1 | sed 's/ /_/g' | uniq -c | awk '{ print $2 "," $1 }'
}

sanitize_item_short() {
    local item="$1"

    case $item in
        amdgcn) item="gcn";;
        c-sky) item="csky";;
        m68k-motorola-sysv) item="m68k";;
        powerpcspe) item="powerpc";;
        rs6000*) item="rs6000";;
        x86-64) item="x86_64";;
        cygwin) item="mingw";;
        GNU/Hurd) item="ONLY_FILES_IN_LIBGO";;
        c++) item="cp";;
        objective-c/c++) item="objcp";;
        libgo) item="IGNORE_FOR_NOW";;
        config.sub/config.guess) item="WILL_FAIL_AND_DOES_NOT_MATTER";;
    esac

    echo "$item"
}

get_item_short() {
    local item=$(echo $line | cut -d ',' -f 1 | cut -d '_' -f 1)

    sanitize_item_short "$item"
}

get_item_full() {
    local item=$(echo $line | cut -d ',' -f 1)

    echo "$item"
}

get_nb_maintainers() {
    local line="$1"
    local item="$2"
    local field
    local nb

    case $item in
        mingw)
            field=3;;
        *)
            field=2;;
    esac

    nb=$(echo $line | cut -d ',' -f $field)
    echo $nb
}

get_src_dirs() {
    local item="$1"
    local exclude_list="$2"
    local excl
    local src_dirs

    if test -n "$exclude_list"; then
        excl="-not ("
        for exclude in $exclude_list; do
            excl="$excl -path $exclude"
        done
        excl="$excl -prune )"
    fi

    if is_lang_frontend $item; then
        src_dirs=$(find ./gcc -type d $excl -iname $item)
    else
        src_dirs=$(find . -type d $excl -iname \*$item\*)
    fi

    echo "$src_dirs"
}

is_lang_frontend() {
    local item="$1"

    case $item in
        C|Ada|c++|D|go|objective-c)
            return 0;;
        *)
            return 1;;
    esac
}


parse_section() {
    local section="$1"
    local item_maintainer_list=$(count_items_maintainers "$section")

    for line in $item_maintainer_list; do
        item_full=$(get_item_full "$line")
        item_short=$(get_item_short "$line")
        nb_maintainers=$(get_nb_maintainers "$line" "$item_short")
        src_dirs=$(get_src_dirs "$item_short" "./libffi")

        if test -z "$src_dirs"; then
            echo "$item_full failed!"
        else
            loc=$(cloc --csv $src_dirs | grep SUM | cut -d ',' -f 5)
            printf "%d,%s,%d,%d\n" $((loc / nb_maintainers)) $item_full $nb_maintainers $loc
        fi

    done
}

echo "LOC-per-maintainer,item,nb-maintainers,LOC"

section="$(extract_section "CPU Port Maintainers" "OS Port Maintainers")"
parse_section "$section"

section="$(extract_section "OS Port Maintainers" "Language Front Ends Maintainers")"
parse_section "$section" port

section="$(extract_section "Language Front Ends Maintainers" "Various Maintainers")"
parse_section "$section" port

section="$(extract_section "Various Maintainers" "Note that individuals who maintain parts")"
parse_section "$section" port

