CFLAGS = -Wall -Wextra -Og -g

.PHONY: all clean

binaries = scan-xml

all: $(binaries)

scan-xml: scan-xml.c

clean:
	$(RM) $(binaries)

