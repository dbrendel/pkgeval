#!/bin/sh

if test -z "$1"; then
    echo "No package given"
    exit 1
fi
package=$1

check_if_installed() {
    local binary="$1"
    if ! command -v $binary >/dev/null; then
        echo "Please install '$binary'!"
        exit 1
    fi
}

time_to_stable() {
    local build=$1
    local reply

    reply="$(bodhi updates query --builds $build)"

    start=$(echo "$reply" | grep Submitted | awk '{ print $2 }')
    pushed=$(echo "$reply" | grep -B1 "has been pushed to stable" | head -1 | awk '{ print $3 }')

    if test -n "$start" && test -n "$pushed"; then
        date_diff "$start" "$pushed"
    else
        echo "NaN"
    fi
}

date_diff() {
    local d1
    local d2
    local d_diff

    d1=$(date -d $1 +%s)
    d2=$(date -d $2 +%s)
    d_diff=$(((d2 - d1) / (60*60*24)))

    echo $d_diff
}

get_builds() {
    local package="$1"
    local num_rows=25

    # A bit ugly but serves its purpose
    while test $(echo "$builds_list" | wc -l) -lt 3 && test $num_rows -lt 100; do
        builds_list=$(bodhi updates query --packages $package --rows $num_rows --status stable | grep "^ $package.*fc35.*" | cut -d ' ' -f 2)
        num_rows=$((num_rows + 25))
    done

    echo "$builds_list"
}

check_if_installed bodhi
check_if_installed bc

builds=$(get_builds $package)
if test -z "$builds"; then
    echo "Could not find any builds!"
    exit 0
fi

sum=0
cnt=0
for build in $builds; do
    days=$(time_to_stable $build)
    if test "$days" = "NaN"; then
        echo "Could not parse $build, skipping.."
        continue
    fi
    printf "%s: %2d %s\n" "$build took" $days "days"
    sum=$(( sum + days ))
    cnt=$((cnt + 1))
done

if test $sum -gt 0 || test $cnt -gt 0; then
    printf "%s %1.1f %s\n" "Average:" $( echo $sum / $cnt | bc -l) "days"
fi

